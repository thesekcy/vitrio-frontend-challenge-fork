# Feedback do projeto

## O que fiz
- No geral acredito que a maioria dos critérios foram atendidos.

## O que não consegui fazer
- Uma das regras era utilizar o endpoint "https://bitbucket.org/vitriosdev/vitrio-dev-test/raw/master/data/catalogo.json", porem o mesmo não era acessivel durante o desenvolvimento como na print abaixo, então fiz o commit do código no BitBucket e deixei o repositorio publico, e utilizei o endpoint do raw do arquivo no meu repositório, "https://bitbucket.org/thesekcy/vitrio-frontend-challenge/raw/4ebfa4614ed36e54cff6c9668277fe80fea6a4b9/data/catalogo.json".
https://prnt.sc/13z73kc

- um detalhe que talvez eu tenha feito errado, foi na descrição do parcelamento dos produtos, tentando entender a API, vi que ele vinha com alguns valores e quantidade de parcelamento variando, dependendo da bandeira do cartão e etc, e na print ele sempre se mantia em 10x, o que eu acabei fazendo no final foi pegando o valor do produto, fazendo a divisão dele por 10x e colocando o texto fixo.

## Considerações finais sobre o projeto
Conte para nós aqui o que você sentiu ao fazer o projeto e quais dificuldades teve ao longo do desenvolvimento...

- Tive um pouco de dificuldade com Typescript, já que é algo que não está presente no meu dia a dia, porem já vinha "namorando" ele a algum tempo querendo aprender mais, foi uma ótima oportunidade para estudar um pouco mais sobre.
- Falando um pouco mais sobre o projeto, inicialmente achei a API um pouco confusa, e diria que se eu fosse desenvolver ela, deixaria algumas informações disponiveis em camadas mais externas para esse tipo de listagem, como por exemplo a imagem do produto.