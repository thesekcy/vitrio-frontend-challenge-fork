interface IProps {
  readonly image: string;
  readonly name: string;
  readonly price: number;
  readonly installments: number;
  readonly consult: boolean;
  readonly link: string;
}

export default function CardRelogio(props: IProps): JSX.Element {
  const { image, name, price, installments, consult, link } = props;

  function convertToReal(value: number) {
    return value.toLocaleString("pt-br", {
      style: "currency",
      currency: "BRL",
    });
  }
  var price_formated = convertToReal(price);
  var installments_formated = convertToReal(installments / 10);

  return (
    <div className="card">
      <img className="product_image" alt={name} src={image} />
      <h3 className="product_title">{name}</h3>
      {consult ? (
        <a className="button btn_consult" href={link}>
          Consulte
        </a>
      ) : (
        <>
          <h4 className="product_price">{price_formated}</h4>
          <h4 className="product_installments">
            10x de {installments_formated} sem juros
          </h4>
          <a className="button btn_buy" href={link}>
            Comprar
          </a>
        </>
      )}
    </div>
  );
}
