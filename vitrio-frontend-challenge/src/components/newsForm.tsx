import { useState } from "react";
import { useForm } from "./../services/formValidate";

interface INews {
  name: string;
  email: string;
}

export default function NewsForm() {
  const [success, setSuccess] = useState(false);
  const {
    handleSubmit,
    handleChange,
    data: news,
    errors,
  } = useForm<INews>({
    validations: {
      name: {
        pattern: {
          value: "^[a-zA-Z-]+$",
          message: "Por favor digite um nome válido.",
        },
      },
    },
    onSubmit: () => {
      setSuccess(true);
      news.name = "";
      news.email = "";
    },
  });

  return (
    <div className="content">
      <h3>Assine nossa newsletter</h3>
      <h5>
        Fique por dentro das nossas novidades e receba 10% de desconto na
        primeira compra.
      </h5>
      <h6>
        * valido somente para joias e não acumulativo com outras promoções
      </h6>
      <form onSubmit={handleSubmit}>
        {success ? (
          <p className="success">Cadastro realizado com sucesso!</p>
        ) : (
          ""
        )}

        <input
          type="text"
          value={news.name || ""}
          onChange={handleChange("name")}
          placeholder="Seu nome"
          required
        />
        <input
          type="email"
          value={news.email || ""}
          onChange={handleChange("email")}
          placeholder="Seu e-mail"
          required
        />
        <button type="submit">Enviar</button>

        {errors.name && <p className="error">{errors.name}</p>}
        {errors.email && <p className="error">{errors.email}</p>}
      </form>
    </div>
  );
}
