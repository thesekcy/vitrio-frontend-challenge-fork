import { useEffect, useState } from "react";
import axios from "axios";

import "./style/App.scss";

import CardRelogio from "./components/cardRelogio";
import NewsForm from "./components/newsForm";

function App() {
  const [productData, setProductData] = useState([]);

  useEffect(() => {
    let url =
      "https://bitbucket.org/thesekcy/vitrio-frontend-challenge/raw/4ebfa4614ed36e54cff6c9668277fe80fea6a4b9/data/catalogo.json";

    axios
      .get(url)
      .then(function (response) {
        setProductData(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  return (
    <>
      <div className="list">
        <div className="container">
          {productData.map((item) => {
            console.log(item["sobconsulta"][0]);
            return (
              <CardRelogio
                key={item["productId"]}
                name={item["productName"]}
                image={item["items"][0]["images"][0]["imageUrl"]}
                price={
                  item["items"][0]["sellers"][0]["commertialOffer"]["Price"]
                }
                installments={
                  item["items"][0]["sellers"][0]["commertialOffer"]["Price"]
                }
                consult={item["sobconsulta"][0] === "sim" ? true : false}
                link={item["link"]}
              />
            );
          })}
        </div>
      </div>
      <div className="news">
        <div className="container">
          <NewsForm />
        </div>
      </div>
    </>
  );
}

export default App;
